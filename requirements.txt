numpy==1.11.2
Pillow==3.4.2
qrcode==5.3
qrtools==0.0.1
six==1.10.0
zbar==0.10
